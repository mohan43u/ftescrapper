### FteScrapper

Simple tool to extract details about books available in [FreeTamilEbooks.com](https://freetamilebooks.com). This tool was created to collect book contributors details for freetamilebooks.com project.

#### Compile
```console
make
```

#### Run
##### csv output
```console
mod/ftescrapper/ftescrapper/ftescrapper nodaemon
```
##### webservice (default port :8888)
```console
mod/ftescrapper/ftescrapper/ftescrapper daemon
```
##### podman webservice (default port container-name:8888)
```console
(cd container; podman-compose up)
```

This command will fetch details about ebooks with following details

* totaldownloadedtimes
* epub_downloadedtimes
* mobi_downloadedtimes
* afour_downloadedtimes
* sixinch_downloadedtimes
* bookno
* bookurl
* epuburl
* mobiurl
* afoururl
* sixinchurl
* emails
* author

**Warning**: This tool was never checked for accuracy of the details. The main reason I developed this tool is to know the book release number and email of the authors who wrote the books.