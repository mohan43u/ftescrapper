all: ftescrapper

ftescrapper:
	(cd mod/ftescrapper/ftescrapper; CGO_ENABLED=0 go build)

install:
	install -D mod/ftescrapper/ftescrapper/ftescrapper $(DESTDIR)/usr/bin/ftescrapper

.PHONY: ftescrapper install
