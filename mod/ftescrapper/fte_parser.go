package ftescrapper

import (
	"io"
	"log"
	"net/http"
	neturl "net/url"
	"strconv"
	"strings"

	"golang.org/x/net/html"
)

type FteParser struct {
	logger *log.Logger
	tokenizer *html.Tokenizer
	bookurls []string
}

func NewFteParser() *FteParser {
	self := &FteParser{}
	self.logger = log.Default()
	return self
}

func (self *FteParser) Parse(url string) string {
	self.bookurls = make([]string, 0)
	lastpagenum := 0
	nexturl := ""

	response, err := http.Get(url)
	if err != nil || response.StatusCode != 200 {
		self.logger.Print("Failed to get ", url, " ", response)
		return nexturl
	}
	self.tokenizer = html.NewTokenizer(response.Body)
	loop := true
	for loop {
		switch self.tokenizer.Next() {
		case html.ErrorToken:
			if self.tokenizer.Err() != io.EOF {
				self.logger.Print("Error occurred during parsing: ",
					self.tokenizer.Err().Error())
			}
			loop = false
		case html.StartTagToken:
			token := self.tokenizer.Token()
			for _, attribute := range token.Attr {
				if strings.Contains(attribute.Val, "/ebooks/") && len(token.Attr) == 1 {
					self.bookurls = append(self.bookurls, attribute.Val)
				}
				if strings.Contains(attribute.Val, "/page/") {
					parsedurl, err := neturl.Parse(attribute.Val)
					if err != nil {
						self.logger.Print("failed to parse page url ", attribute.Val)
						loop = false
					} else {
						path := parsedurl.EscapedPath()
						if strings.HasSuffix(path, "/") { path = path[:len(path) - 1] }
						pathv := strings.Split(path, "/")
						pagenum, _ := strconv.Atoi(pathv[len(pathv) - 1])
						if lastpagenum < pagenum {
							lastpagenum = pagenum
						}
					}
				}
			}
		}
	}
	if lastpagenum > 0 {
		parsedurl,err := neturl.Parse(url)
		if err != nil {
			self.logger.Print("Failed to parse url", err)
		} else {
			path := parsedurl.EscapedPath()
			if strings.Contains(path, "/page/") {
				if strings.HasSuffix(path, "/") { path = path[:len(path) - 1] }
				pathv := strings.Split(path, "/")
				pagenum, _ := strconv.Atoi(pathv[len(pathv) - 1])
				if pagenum > 1 && pagenum < lastpagenum {
					newpathv := make([]string, 0)
					newpathv = append(newpathv, pathv[:len(pathv) - 1]...)
					newpathv = append(newpathv, strconv.Itoa(pagenum + 1))
					parsedurl.Path = strings.Join(newpathv, "/")
					nexturl = parsedurl.String()
				}
			} else {
				pagenum := 2
				if pagenum < lastpagenum {
					newpathv := make([]string, 0)
					newpathv = append(newpathv, "/page", strconv.Itoa(pagenum))
					parsedurl.Path = strings.Join(newpathv, "/")
					nexturl = parsedurl.String()
				}
			}
		}
	}
	return nexturl
}

func (self *FteParser) GetBookUrls() []string {
	return self.bookurls
}
