package ftescrapper

import (
	"errors"
	"io"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"unicode/utf16"

	"github.com/h2non/filetype"
	"golang.org/x/net/html"
)

type FteBookPageParser struct {
	logger *log.Logger
	tokenizer *html.Tokenizer
	epuburl string
	mobiurl string
	afourpdfurl string
	sixinchpdfurl string
	epuburl_downloadedtimes string
	mobiurl_downloadedtimes string
	afourpdfurl_downloadedtimes string
	sixinchpdfurl_downloadedtimes string
	emails []string
	authornames []string
	bookno string
	url string
}

func NewFteBookPageParser(url string) *FteBookPageParser {
	self := &FteBookPageParser{}
	self.logger = log.Default()
	self.url = url
	return self
}

func (self *FteBookPageParser) detectDownloadedTimes() string {
	downloadedtimes := ""
	loop := true
	for loop {
		switch self.tokenizer.Next() {
		case html.ErrorToken:
			if self.tokenizer.Err() != io.EOF {
				self.logger.Print("Error occurred during parsing: ",
					self.tokenizer.Err().Error())
			}
			loop = false
		case html.TextToken:
			ntoken := strings.ToLower(strings.TrimSpace(self.tokenizer.Token().String()))
			if len(ntoken) > 0 {
				downloadinfo := strings.ToLower(ntoken)
				if strings.Contains(downloadinfo, "downloaded") {
					re := regexp.MustCompile("downloaded ([[:digit:]]*) times")
					downloadinfov := re.FindStringSubmatch(downloadinfo)
					if len(downloadinfov) == 2 {
						downloadedtimes = downloadinfov[1]
					}
					loop = false
				}
			}
		}
	}
	return downloadedtimes
}

func (self *FteBookPageParser) detectFileTypeSetUrls(comparestring string, url string) bool {
	ret := false
	if strings.Contains(comparestring, "epub") {
		self.epuburl = url
		self.epuburl_downloadedtimes = self.detectDownloadedTimes()
		ret = true
	} else if strings.Contains(comparestring, "mobi") {
		self.mobiurl = url
		self.mobiurl_downloadedtimes = self.detectDownloadedTimes()
		ret = true
	} else if strings.Contains(comparestring, "inch") {
		self.sixinchpdfurl = url
		self.sixinchpdfurl_downloadedtimes = self.detectDownloadedTimes()
		ret = true
	} else if strings.Contains(comparestring, "pdf") {
		self.afourpdfurl = url
		self.afourpdfurl_downloadedtimes = self.detectDownloadedTimes()
		ret = true
	}
	return ret
}

func (self *FteBookPageParser) detectFileTypeLevelOne(url string) bool {
	ret := false
	response, err := http.Get(url)
	if err != nil || response.StatusCode != 200 {
		self.logger.Print("Failed to get ", self.url, " ", response)
		return ret
	}
	fileheader := make([]byte, 261)
	if rbytes, err := response.Body.Read(fileheader); err != nil {
		self.logger.Print("Failed to read fileheader ", err)
		return ret
	} else {
		if rfiletype, err := filetype.Match(fileheader[:rbytes]); err != nil {
			self.logger.Print("Failed to match filetype ", err)
			return ret
		} else {
			switch rfiletype.Extension {
			case "zip":
				self.epuburl = url
				self.epuburl_downloadedtimes = self.detectDownloadedTimes()
			case "pdf":
				if strings.Contains(url, "inch") {
					self.sixinchpdfurl = url
					self.sixinchpdfurl_downloadedtimes = self.detectDownloadedTimes()
				} else if len(self.afourpdfurl) == 0 {
					self.afourpdfurl = url
					self.afourpdfurl_downloadedtimes = self.detectDownloadedTimes()
				} else {
					self.sixinchpdfurl = url
					self.sixinchpdfurl_downloadedtimes = self.detectDownloadedTimes()
				}
			default:
				fileheaderstr := string(fileheader[:rbytes])
				if strings.Contains(fileheaderstr, "BOOKMOBI") {
					self.mobiurl = url
					self.mobiurl_downloadedtimes = self.detectDownloadedTimes()
				} else {
					self.logger.Print("unknown file: ", rfiletype, fileheader[:rbytes])
					return ret
				}
			}
		}
	}
	ret = true
	return ret
}

func (self *FteBookPageParser) detectFileTypeLevelZero(url string) {
	loop := true
	for loop {
		switch self.tokenizer.Next() {
		case html.ErrorToken:
			if self.tokenizer.Err() != io.EOF {
				self.logger.Print("Error occurred during parsing: ",
					self.tokenizer.Err().Error())
			}
			loop = false
		case html.TextToken:
			ntoken := strings.ToLower(strings.TrimSpace(self.tokenizer.Token().String()))
			if len(ntoken) > 0 {
				urltext := strings.ToLower(ntoken)
				if !self.detectFileTypeSetUrls(urltext, url) {
					self.detectFileTypeLevelOne(url)
				}
				loop = false
			}
		}
	}
}

func (self *FteBookPageParser) detectFileType(url string) bool {
	ret := false
	if !self.detectFileTypeSetUrls(url, url) {
		self.detectFileTypeLevelZero(url)
	}
	return ret
}

func (self *FteBookPageParser) decryptEmail(data string) string {
	email := ""
	dec_int := make([]uint16, 0)
	key, err := strconv.ParseUint(data[:2], 16, 16)
	if err != nil {
		self.logger.Print("Failed to get key ", err)
		return email
	}
	key16 := uint16(key)

	for index := 0; index < len(data); index += 2 {
		enc_int, err := strconv.ParseUint(data[index:index + 2], 16, 16)
		if err != nil {
			self.logger.Print("Failed to get encrypted int ", err)
			return email
		}
		enc_int16 := uint16(enc_int)
		dec_int = append(dec_int, uint16(enc_int16 ^ key16))
	}
	r := utf16.Decode(dec_int)
	email = string(r)[1:]
	return email
}

func (self *FteBookPageParser) detectBookNo(textstring string) bool {
	ret := false
	if strings.Contains(textstring, `புத்தக எண்`) {
		re := regexp.MustCompile(`புத்தக எண் – ([[:digit:]]*)`)
		booknov := re.FindStringSubmatch(textstring)
		if len(booknov) == 2 {
			self.bookno = booknov[1]
			ret = true
		}
	}
	return ret
}

func (self *FteBookPageParser) detectAuthors(textstring string) bool {
	ret := false
	matched, _ := regexp.Match(`ஆசிரியர் +.{1} +`, []byte(textstring))
	if matched {
		re := regexp.MustCompile(`ஆசிரியர் +.{1} +((?:[^[:blank:]–&:;#]+[[:blank:]]?)+)`)
		authornamev := re.FindStringSubmatch(textstring)
		if len(authornamev) > 1 {
			authornames := strings.Join(authornamev[1:], " ")
			self.authornames = append(self.authornames, strings.TrimSpace(strings.ReplaceAll(authornames, ",", " ")))
			ret = true
		}
	}
	return ret
}

func (self *FteBookPageParser) Parse() bool {
	ret := false

	response, err := http.Get(self.url)
	if err != nil || response.StatusCode != 200 {
		self.logger.Print("Failed to get ", self.url, " ", response)
		return ret
	}
	self.tokenizer = html.NewTokenizer(response.Body)
	loop := true
	for loop {
		switch self.tokenizer.Next() {
		case html.ErrorToken:
			if self.tokenizer.Err() != io.EOF {
				self.logger.Print("Error occurred during parsing: ",
					self.tokenizer.Err().Error())
			}
			loop = false
		case html.StartTagToken:
			token := self.tokenizer.Token()
			for _, attribute := range token.Attr {
				if strings.Contains(attribute.Val, "/?tmstv=") &&
					!strings.Contains(attribute.Val, "send2kindle") {
					self.detectFileType(attribute.Val)
				}
				if strings.Contains(attribute.Val, "/cdn-cgi/l/email-protection#") {
					encemail := strings.Split(attribute.Val, "/cdn-cgi/l/email-protection#")[1]
					self.emails = append(self.emails, self.decryptEmail(encemail))
				}
				if strings.Contains(attribute.Key, "data-cfemail") {
					self.emails = append(self.emails, self.decryptEmail(attribute.Val))
				}
			}
		case html.TextToken:
			ntoken := strings.ToLower(strings.TrimSpace(self.tokenizer.Token().String()))
			if len(ntoken) > 0 {
				self.detectBookNo(ntoken)
				self.detectAuthors(ntoken)
			}
		}
	}
	ret = true
	return ret
}

func (self *FteBookPageParser) GetBookUrl() string {
	return self.url
}

func (self *FteBookPageParser) GetBookNo() (int, error) {
	return strconv.Atoi(self.bookno)
}

func (self *FteBookPageParser) GetAuthorNames() []string {
	return self.authornames
}

func (self *FteBookPageParser) GetEmails() []string {
	return self.emails
}

func (self *FteBookPageParser) GetEpubUrl() (string) {
	return self.epuburl
}

func (self *FteBookPageParser) GetEpubDownloadedTimes() (int, error) {
	return strconv.Atoi(self.epuburl_downloadedtimes)
}

func (self *FteBookPageParser) GetMobiUrl() (string) {
	return self.mobiurl
}

func (self *FteBookPageParser) GetMobiDownloadedTimes() (int, error) {
	return strconv.Atoi(self.mobiurl_downloadedtimes)
}

func (self *FteBookPageParser) GetAfourPdfUrl() (string) {
	return self.afourpdfurl
}

func (self *FteBookPageParser) GetAfourPdfDownloadedTimes() (int, error) {
	return strconv.Atoi(self.afourpdfurl_downloadedtimes)
}

func (self *FteBookPageParser) GetSixInchPdfUrl() (string) {
	return self.sixinchpdfurl
}

func (self *FteBookPageParser) GetSixInchPdfDownloadedTimes() (int, error) {
	return strconv.Atoi(self.sixinchpdfurl_downloadedtimes)
}

func (self *FteBookPageParser) GetTotalDownloadedTimes() (int, error) {
	epub, err0 := self.GetEpubDownloadedTimes()
	mobi, err1 := self.GetMobiDownloadedTimes()
	afour, err2 := self.GetAfourPdfDownloadedTimes()
	sixinch, err3 := self.GetSixInchPdfDownloadedTimes()
	return (epub + mobi + afour + sixinch), errors.Join(err0, err1, err2, err3)
}
