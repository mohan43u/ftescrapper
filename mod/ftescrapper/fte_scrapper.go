package ftescrapper

import (
	"encoding/csv"
	"encoding/json"
	"log"
	"strconv"
	"strings"
	"sync"
)

type FteBookInfo struct {
	BookNo int
	BookUrl string
	Authors []string
	Emails []string
	EpubUrl string
	MobiUrl string
	AfourPdfUrl string
	SixInchPdfUrl string
	EpubDownloadedTimes int
	MobiDownloadedTimes int
	AfourPdfDownloadedTimes int
	SixInchPdfDownloadedTimes int
	TotalDownloadedTimes int
}

func GetFteBookInfoHeader() []string {
	return []string{
		"BookNo",
		"TotalDownloadedTimes",
		"EpubDownloadedTimes",
		"MobiDownloadedTimes",
		"AfourPdfDownloadedTimes",
		"SixInchPdfDownloadedTimes",
		"BookUrl",
		"EpubUrl",
		"MobiUrl",
		"AfourPdfUrl",
		"SixInchPdfUrl",
		"Authors",
		"Emails",
	}
}

func (self *FteBookInfo) Strings() []string {
	return []string{
		strconv.Itoa(self.BookNo),
		strconv.Itoa(self.TotalDownloadedTimes),
		strconv.Itoa(self.EpubDownloadedTimes),
		strconv.Itoa(self.MobiDownloadedTimes),
		strconv.Itoa(self.AfourPdfDownloadedTimes),
		strconv.Itoa(self.SixInchPdfDownloadedTimes),
		self.BookUrl,
		self.EpubUrl,
		self.MobiUrl,
		self.AfourPdfUrl,
		self.SixInchPdfUrl,
		strings.Join(self.Authors, "~"),
		strings.Join(self.Emails, "~"),
	}
}

func (self *FteBookInfo) String() string {
	return strings.Join(self.Strings(), ",")
}

type FteScrapper struct {
	logger *log.Logger
	Books map[string]FteBookInfo
	BooksInfoMutex sync.Mutex
}

func GetFteScrapper() *FteScrapper {
	ftescrapper := &FteScrapper{}
	ftescrapper.Books = make(map[string]FteBookInfo)
	ftescrapper.logger = log.Default()
	return ftescrapper
}

func (self *FteScrapper) CollectBooksInfo(url string) bool {
	if len(url) < 0 {
		self.logger.Print("invalid url", url)
		return false
	}

	wait := &sync.WaitGroup{}
	fteparser := NewFteParser()
	for len(url) > 0 {
		url = fteparser.Parse(url)
		for _, bookurl := range fteparser.GetBookUrls() {
			wait.Add(1)
			go func(bookurl string, wait *sync.WaitGroup) {
				defer wait.Done()
				bookpageparser := NewFteBookPageParser(bookurl)
				if bookpageparser.Parse() == false {
					self.logger.Print("failed to parse book information from", bookurl)
					return
				}

				var bookinfo FteBookInfo
				bookinfo.BookUrl = bookpageparser.GetBookUrl()
				bookinfo.BookNo, _ = bookpageparser.GetBookNo()
				bookinfo.Authors = bookpageparser.GetAuthorNames()
				bookinfo.Emails = bookpageparser.GetEmails()
				bookinfo.EpubUrl = bookpageparser.GetEpubUrl()
				bookinfo.MobiUrl = bookpageparser.GetMobiUrl()
				bookinfo.AfourPdfUrl = bookpageparser.GetAfourPdfUrl()
				bookinfo.SixInchPdfUrl = bookpageparser.GetSixInchPdfUrl()
				bookinfo.EpubDownloadedTimes, _ = bookpageparser.GetEpubDownloadedTimes()
				bookinfo.MobiDownloadedTimes, _ = bookpageparser.GetMobiDownloadedTimes()
				bookinfo.AfourPdfDownloadedTimes, _ = bookpageparser.GetAfourPdfDownloadedTimes()
				bookinfo.SixInchPdfDownloadedTimes, _ = bookpageparser.GetSixInchPdfDownloadedTimes()
				bookinfo.TotalDownloadedTimes, _ = bookpageparser.GetTotalDownloadedTimes()
				self.BooksInfoMutex.Lock()
				self.Books[bookinfo.BookUrl] = bookinfo
				self.BooksInfoMutex.Unlock()
			}(bookurl, wait)
		}
	}
	wait.Wait()

	var books_count int
	self.BooksInfoMutex.Lock()
	books_count = len(self.Books)
	self.BooksInfoMutex.Unlock()
	if books_count <= 0 {
		self.logger.Print("empty booksinfo")
		return false
	}

	return true
}

func (self *FteScrapper) GetBooksInfo() map[string]FteBookInfo {
	var books map[string]FteBookInfo

	self.BooksInfoMutex.Lock()
	books = self.Books
	self.BooksInfoMutex.Unlock()
	return books
}

func (self *FteScrapper) GetCsv() string {
	var csvstring strings.Builder

	csvwriter := csv.NewWriter(&csvstring)
	if csvwriter == nil {
		self.logger.Print("cannot create csvwriter")
		return csvstring.String()
	}

	if err := csvwriter.Write(GetFteBookInfoHeader()); err != nil {
		self.logger.Print("Failed to write header", err)
		return csvstring.String()
	}

	for _, bookinfo := range self.GetBooksInfo() {
		csvwriter.Write(bookinfo.Strings())
	}

	return csvstring.String()
}

func (self *FteScrapper) GetJson() string {
	var jsonstring strings.Builder

	jsonencoder := json.NewEncoder(&jsonstring)
	if jsonencoder == nil {
		self.logger.Print("failed to create jsonencoder")
		return jsonstring.String()
	}

	booksinfo := self.GetBooksInfo()
	if err := jsonencoder.Encode(booksinfo); err != nil {
		self.logger.Print("failed to encode booksinfo", err)
		return jsonstring.String()
	}

	return jsonstring.String()
}
