package ftescrapper

import (
	"context"
	"log"
	"net/http"
	"sync"
	"time"
)

type FteService struct {
	url string
	logger *log.Logger
	scrapper *FteScrapper
	wg *sync.WaitGroup
	shutdown bool
	server *http.Server
}

func GetFteService() *FteService {
	fteservice := &FteService{}
	fteservice.logger = log.Default()
	fteservice.scrapper = GetFteScrapper()
	fteservice.wg = &sync.WaitGroup{}
	fteservice.shutdown = false
	return fteservice
}

func (self *FteService) Init(url string, hostport string) bool {
	self.url = url
	self.server = &http.Server{Addr: hostport}
	return true
}

func (self *FteService) Run() bool {
	self.wg.Add(1)
	go func() {
		defer func() {
			self.logger.Print("exit from collectbookinfo goroutine")
			self.wg.Done()
		}()

		for loop := true; loop; {
			self.logger.Println("Starting info gathering at ", time.Now())
			if self.scrapper.CollectBooksInfo(self.url) == false {
				self.shutdown = true
				loop = false
				return
			}
			self.logger.Println("Finished info gathering at ", time.Now())
			time.Sleep(time.Hour * 24)
			self.logger.Println("Wait Interval completed. currenttime: ", time.Now())
		}
	}()

	self.wg.Add(1)
	go func() {
		defer func() {
			self.logger.Print("exit from http goroutine")
			self.wg.Done()
		}()

		http.Handle("/books.json",
			http.HandlerFunc(func(response http.ResponseWriter,
				request *http.Request) {
				response.Write([]byte(self.scrapper.GetJson()))
			}))

		http.Handle("/",
			http.HandlerFunc(func(response http.ResponseWriter,
				request *http.Request) {
				html := "<html>\n" +
					"<head>\n" +
					"<title>FTE Stats</title>\n" +
					"<script>\n" +
					"let responseText = \"\";\n" +
					"let sortfield = \"BookNo\";\n" +
					"let reDrawing = false;\n" +
					"let reDraw = function() {\n" +
					"reDrawing = true;\n" +
					"let tablediv = document.getElementById(\"table\");\n" +
					"let innerHTML = \"<table>\";\n" +
					"innerHTML += \"<tbody>\";\n" +
					"let books = JSON.parse(responseText);\n" +
					"books = Object.values(books);\n" +
					"books.sort(function(a, b) {\n" +
					"return a[sortfield] == b[sortfield] ? 0 : (a[sortfield] < b[sortfield] ? 1 : -1);\n" +
					"});\n" +
					"let is_headers_printed = false;\n" +
					"for (book of books) {\n" +
					"if (is_headers_printed == false) {\n" +
					"innerHTML += \"<tr>\";\n" +
					"innerHTML += \"<td>\";\n" +
					"innerHTML += \"Book No\";\n" +
					"innerHTML += \"<a id='sortfield_BookNo' href='#' onclick='setSortField(this);'>&#x2193</a>\";\n" +
					"innerHTML += \"</td>\";\n" +
					"innerHTML += \"<td>Author</td>\";\n" +
					"innerHTML += \"<td>Book</td>\";\n" +
					"innerHTML += \"<td>\";\n" +
					"innerHTML += \"Total Downloaded Times\";\n" +
					"innerHTML += \"<a id='sortfield_TotalDownloadedTimes' href='#' onclick='setSortField(this);'>&#x2193</a>\";\n" +
					"innerHTML += \"</td>\";\n" +
					"innerHTML += \"<td>Epub Downloaded Times</td>\";\n" +
					"innerHTML += \"<td>Mobi Downloaded Times</td>\";\n" +
					"innerHTML += \"<td>Afour Pdf Downloaded Times</td>\";\n" +
					"innerHTML += \"<td>Six Inch Pdf Downloaded Times</td>\";\n" +
					"innerHTML += \"</tr>\";\n" +
					"is_headers_printed = true;\n" +
					"}\n" +
					"let bookurlparser = new URL(book.BookUrl);\n" +
					"let bookname = bookurlparser.pathname;\n" +
					"bookname = bookname[bookname.length - 1] == '/' ? bookname.slice(0, length - 1) : bookname;\n" +
					"bookname = bookname.slice(bookname.lastIndexOf('/') + 1, bookname.length);\n" +
					"innerHTML += \"<tr>\";\n" +
					"innerHTML += \"<td>\" + book.BookNo + \"</td>\";\n" +
					"innerHTML += \"<td>\" + (book.Authors != null && book.Authors.length > 0 ? book.Authors[0] : \"\") + \"</td>\";\n" +
					"innerHTML += \"<td><a href='\" + book.BookUrl + \"' target='_blank'>\" + decodeURIComponent(bookname) + \"</a></td>\";\n" +
					"innerHTML += \"<td>\" + book.TotalDownloadedTimes.toString() + \"</td>\";\n" +
					"innerHTML += \"<td><a href='\" + book.EpubUrl + \"' target='_blank'>\" + book.EpubDownloadedTimes.toString() + \"</a></td>\";\n" +
					"innerHTML += \"<td><a href='\" + book.MobiUrl + \"' target='_blank'>\" + book.MobiDownloadedTimes.toString() + \"</a></td>\";\n" +
					"innerHTML += \"<td><a href='\" + book.AfourPdfUrl + \"' target='_blank'>\" + book.AfourPdfDownloadedTimes.toString() + \"</a></td>\";\n" +
					"innerHTML += \"<td><a href='\" + book.SixInchPdfUrl + \"' target='_blank'>\" + book.SixInchPdfDownloadedTimes.toString() + \"</a></td>\";\n" +
					"innerHTML += \"</tr>\";\n" +
					"}\n" +
					"innerHTML += \"</tbody>\";\n" +
					"innerHTML += \"</table>\";\n" +
					"tablediv.innerHTML = innerHTML;\n" +
					"reDrawing = false;\n" +
					"}\n" +
					"let setSortField = function(element) {\n" +
					"sortfield = element.id.replace('sortfield_', '');\n" +
					"if (reLoading == false) { reLoad(); }\n" +
					"}\n" +
					"let reLoading = false;\n" +
					"let reLoad = function() {\n" +
					"reLoading = true;\n" +
					"let request = new XMLHttpRequest();\n" +
					"request.addEventListener(\"load\", function() {\n" +
					"responseText = request.responseText;\n" +
					"if (reDrawing == false) { reDraw(); }\n" +
					"reLoading = false;\n" +
					"});\n" +
					"request.open(\"GET\", \"/books.json\");\n" +
					"request.send();\n" +
					"}\n" +
					"window.addEventListener(\"load\", function() {\n" +
					"reLoad();\n" +
					"});\n" +
					"</script>\n" +
					"</head>\n" +
					"<body>\n" +
					"<div id=\"table\"></div>\n" +
					"</body>\n" +
					"</html>\n"
				response.Write([]byte(html))
			}))

		self.wg.Add(1)
		go func() {
			defer func() {
				self.logger.Print("exit from http shutdown goroutine")
				self.wg.Done()
			}()

			for loop := true; loop; {
				if self.shutdown {
					if err := self.server.Shutdown(context.TODO()); err != nil {
						self.logger.Print("failed to shutdown properly", err)
					}
					loop = false
					continue
				}
				time.Sleep(time.Second * 10)
			}
		}()

		if err := self.server.ListenAndServe(); err != nil {
			self.logger.Print("ListenAndServe() failed ", err)
			self.shutdown = true
			return
		}
	}()

	self.wg.Wait()
	return true
}
