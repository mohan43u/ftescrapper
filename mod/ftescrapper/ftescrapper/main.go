package main

import (
	"fmt"
	"os"
	"strings"

	ftescrappermod "gitlab.com/mohan43u/ftescrapper/mod/ftescrapper"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("[usage] " + os.Args[0] + " <daemon[:host:port]|nodaemon> [url]")
		return
	}
	url := os.Getenv("FTESCRAPPER_URL")
	if len(url) <= 0 { url = "https://freetamilebooks.com" }
	if len(os.Args) > 2 { url = os.Args[2] }

	mode := os.Args[1]
	if strings.HasPrefix(mode, "daemon") {
		hostport := os.Getenv("FTESCRAPPER_HOSTPORT")
		if len(hostport) <= 0 {	hostport = "127.0.0.1:8888" }
		fields := strings.Split(mode, ":")
		if len(fields) >= 3 {
			host := fields[1]
			port := fields[2]
			hostport = strings.Join([]string{host, port}, ":")
		}

		fteservice := ftescrappermod.GetFteService()
		fteservice.Init(url, hostport)
		fteservice.Run()
		return
	}

	ftescrapper := ftescrappermod.GetFteScrapper()
	if ftescrapper.CollectBooksInfo(url) == false {
		fmt.Fprint(os.Stderr, "failed to collect books information from", url)
		return
	}
	fmt.Print(ftescrapper.GetCsv())
}
